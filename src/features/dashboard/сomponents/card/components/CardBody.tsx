import * as React from "react";
import { Status } from "model/DashboardModel";
import styles from "./CardBody.module.scss";
import { useStatusClass } from "features/common/Hooks";
import { CardContext } from "../context/CardContext";
import { useContext } from "react";

const CardBody: React.FC = () => {
  const { cardState: { status, timeStamp } } = useContext(CardContext);

  const statusClass = useStatusClass(status);
  return (<div className={styles.cardBody}>
    <div className={`${styles.cardStatus} ${statusClass}`}>{Status[status]} {timeStamp}</div>
  </div>);
};

export default CardBody;