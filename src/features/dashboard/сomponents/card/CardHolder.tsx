import * as React from "react";
import { useContext } from "react";

import styles from "./Card.module.scss";
import CardHeader from "./components/CardHeader";
import CardBody from "./components/CardBody";
import CardFooter from "./components/CardFooter";
import { CardContext } from "./context/CardContext";
import { DraggableCard } from "./components/DraggableCard";
import { useStatusClass } from "features/common/Hooks";

import classNames from "classnames";
import { Status } from "../../../../model/DashboardModel";

const backGroundColor = (status: Status): string => {
  switch (status) {
    case Status.Success:
      return "#222924";
    case Status.Warning:
      return "#383129";
    case Status.Critical:
      return "#342B2D";
    case Status.Unknown:
      return "#242C2E";
  }
};
export const CardHolder: React.FC = () => {
  const { cardIndex, cardState } = useContext(CardContext);
  const additionalStyling = {
    "--animation-order": cardIndex,
    "background": backGroundColor(cardState.status)
  } as React.CSSProperties;
  const statusClass = useStatusClass(cardState.status);

  return (
    <DraggableCard>
      <div className={classNames(styles.card, statusClass)} style={additionalStyling}>
        <CardHeader/>
        <CardBody/>
        <CardFooter/>
      </div>
    </DraggableCard>
  )
};


