import { addToGroup, newGroup } from "app/commonAction";
import {
  DimensionsState,
  DragAndDropCardState,
  ItemDimension,
  Position
} from "model/DashboardModel";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { dragAndDropStateByIdSelector, updateDragAndDropState } from "./dragAndDropSlice";
import { RootState } from "../../app/store";
import { createSelector } from "@reduxjs/toolkit";
import { groupsIdsSelector } from "../dashboard/dashboardSliceSelectors";

const isCardHit = (
  dimensions: Array<ItemDimension>,
  position: Position
): ItemDimension | undefined => {
  const { x, y } = position;
  return dimensions.find((item: ItemDimension) => {
    const { x: itemX, y: itemY, height, width } = item;
    return (
      x >= itemX && x <= itemX + width && y >= itemY && y <= itemY + height
    );
  });
};

const useDragAndDrop = (dimensionsState: DimensionsState, viewId: string) => {
  // const [
  //   dragAndDropState,
  //   setDragAndDropState,
  // ] = useState<DragAndDropCardState>(undefined);

  const existingGroupsIds = useSelector(groupsIdsSelector);
  const dragAndDropState = useSelector<RootState, DragAndDropCardState>((state) => state.dragAndDrop);

  const dispatch = useDispatch();

  const handleMouseMove = React.useCallback(
    ({ clientX: x, clientY: y }) => {
      if (!dragAndDropState?.id) {
        return;
      }

      const translation = {
        x: x - dragAndDropState.origin.x,
        y: y - dragAndDropState.origin.y,
      };

      dispatch(updateDragAndDropState({
        ...dragAndDropState,
        translation,
        isDragging: true,
      }));
    },
    [ dragAndDropState ]
  );

  const handleMouseUp = React.useCallback(
    ({ clientX: x, clientY: y }) => {
      if (!dragAndDropState?.isDragging) {
        console.log("no dragging");
        dispatch(updateDragAndDropState({} as DragAndDropCardState));
        //setDragAndDropState(undefined);
        return;
      }

      const card = isCardHit(dimensionsState, { x, y });

      if (card?.id === dragAndDropState.id) {
        console.log("cannot hit same card");
        //setDragAndDropState(undefined);
        dispatch(updateDragAndDropState({} as DragAndDropCardState));

        return;
      }

      if (card) {
        //setDragAndDropState(undefined);
        dispatch(updateDragAndDropState({} as DragAndDropCardState));

        const groupAction = existingGroupsIds.includes(card.id)
          ? addToGroup(viewId, card.id, dragAndDropState.id)
          : newGroup(viewId, [ card.id, dragAndDropState.id ]);

        dispatch(groupAction);
      }

      //setDragAndDropState(undefined);
      dispatch(updateDragAndDropState({} as DragAndDropCardState));
    },
    [ dimensionsState, dragAndDropState, viewId, existingGroupsIds ]
  );

  const handleMouseDown = React.useCallback(
    ({ clientX: x, clientY: y, target }) => {
      if (target.closest("[non-draggable]")) {
        return;
      }

      const card = isCardHit(dimensionsState, { x, y });

      if (card) {
        // setDragAndDropState({
        //   id: card.id,
        //   origin: { x, y },
        //   translation: { x: 0, y: 0 },
        //   isDragging: true,
        // });

        dispatch(updateDragAndDropState({
          id: card.id,
          origin: { x, y },
          translation: { x: 0, y: 0 },
          isDragging: true,
        }));
      }
    },
    [ dimensionsState ]
  );

  return {
    handleMouseDown,
    handleMouseMove,
    handleMouseUp
  };
};

export default useDragAndDrop;
