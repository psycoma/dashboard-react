import * as React from "react";
import { useContext, useMemo } from "react";
import { CardContext } from "../context/CardContext";
import styles from "../Card.module.scss";
import { useSelector } from "react-redux";
import { RootState } from "app/store";
import { DragAndDropCardState } from "model/DashboardModel";
import { dragAndDropStateByIdSelector } from "../../../../grid/dragAndDropSlice";

export const DraggableCard: React.FC = ({ children }) => {
  const { cardState: { id } } = useContext(CardContext);

  const dragAndDropState = useSelector<RootState, DragAndDropCardState | undefined>(state => dragAndDropStateByIdSelector(state, id));

  const dragAndDropStyles = useMemo((): any => {
    if (dragAndDropState?.id === id) {
      return {
        cursor: dragAndDropState ? "-webkit-grabbing" : "-webkit-grab",
        transform: `translate(${dragAndDropState?.translation.x}px, ${dragAndDropState?.translation?.y}px)`,
        transition: dragAndDropState ? "none" : "transform 500ms",
        zIndex: dragAndDropState ? 5 : 1,
      };
    }
  }, [ id, dragAndDropState ]);

  return (<div
    className={`${styles.dragAndDropContainer}`}
    id={id}
    style={dragAndDropStyles}>
    {children}
  </div>);
};


