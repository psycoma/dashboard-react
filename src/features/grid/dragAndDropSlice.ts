import { createSelector, createSlice, EntityState, PayloadAction, Selector } from "@reduxjs/toolkit";
import { DragAndDropCardState } from "../../model/DashboardModel";
import { RootState } from "../../app/store";

const dragAndDropSlice = createSlice({
  name: "dragAndDrop",
  initialState: {} as DragAndDropCardState,
  reducers: {
    updateDragAndDropState: (state, updateDragAndDrop: PayloadAction<DragAndDropCardState>) => {
      state.id = updateDragAndDrop.payload.id;
      state.isDragging = updateDragAndDrop.payload.isDragging;
      state.origin = updateDragAndDrop.payload.origin;
      state.translation = updateDragAndDrop.payload.translation;
    }
  },
});

export const dragAndDropStateByIdSelector = createSelector(
  (state: RootState, id: string) => state.dragAndDrop.id === id ? state.dragAndDrop : undefined,
  (dragAndDropState) => dragAndDropState);

export const { updateDragAndDropState } = dragAndDropSlice.actions;
export const { reducer: dragAndDrop } = dragAndDropSlice;