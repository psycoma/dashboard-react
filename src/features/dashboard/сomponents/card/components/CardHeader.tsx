import * as React from "react";
import styles from "./CardHeader.module.scss";
import RemoveCard from "./RemoveCard";
import UngroupCard from "./UngroupCard";
import { useContext } from "react";
import { CardContext } from "../context/CardContext";
import { useSelector } from "react-redux";
import { RootState } from "../../../../../app/store";


const CardHeader: React.FC = () => {

  const { cardState } = useContext(CardContext);
  const currentViewId = useSelector((state: RootState) => state.views.currentViewId);

  if (!cardState) {
    return null;
  }

  return (
    <div className={styles.cardHeader}>
      {'content' in cardState ? <UngroupCard/> : <RemoveCard viewId={currentViewId} cardId={cardState.id}/>}
      <div className={styles.cardName}>{cardState.name}</div>
    </div>
  )
};

export default CardHeader;