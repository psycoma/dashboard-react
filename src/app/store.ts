import { Action, combineReducers, configureStore, ThunkAction } from "@reduxjs/toolkit";
import cards from "../features/dashboard/dashboardSlice";
import { views } from "../features/views/viewsSlice";
import { dragAndDrop } from "../features/grid/dragAndDropSlice";

const rootReducer = combineReducers({
  cards,
  views,
  dragAndDrop
});

const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

// if (process.env.NODE_ENV === 'development' && module.hot) {
//   module.hot.accept('./rootReducer', () => {
//     const newRootReducer = require('./rootReducer').default
//     store.replaceReducer(newRootReducer)
//   })
// }

export type AppDispatch = typeof store.dispatch;

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>;

export default store;
