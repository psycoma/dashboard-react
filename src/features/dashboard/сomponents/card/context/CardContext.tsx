import React from "react";
import { CardState, DragAndDropCardState } from "model/DashboardModel";
import { useSelector } from "react-redux";
import { RootState } from "app/store";
import { dashboardSelectors } from "features/dashboard/dashboardSliceReducers";

type CardContextProps = {
  cardState: CardState,
  dragAndDropState?: DragAndDropCardState,
  cardIndex: number,
};

type LoaderProps = {
  cardId: string;
  dragAndDropState?: DragAndDropCardState;
  cardIndex: number;
}

export const CardContext = React.createContext<CardContextProps>({} as CardContextProps);

export const CardDataLoader: React.FC<LoaderProps> = ({ cardId, dragAndDropState, cardIndex, children }) => {
  const cardState = useSelector<RootState, CardState | undefined>((state) =>
    dashboardSelectors.selectById(state.cards, cardId)
  );

  if (!cardState) {
    return null;
  }

  return (<CardContext.Provider value={{
    cardState,
    cardIndex
  }}>{children}</CardContext.Provider>)
};
