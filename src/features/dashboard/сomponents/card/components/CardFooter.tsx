import * as React from "react";
import KpiList from "../../kpi/KpiList";
import styles from "./CardFooter.module.scss";
import { useContext } from "react";
import { CardContext } from "../context/CardContext";


const CardFooter: React.FC = () => {

  const { cardState: { kpis, id } } = useContext(CardContext);

  return (
    <div className={styles.cardFooter}>
      <KpiList kpis={kpis} ciId={id}/>
    </div>
  )
};

export default CardFooter;