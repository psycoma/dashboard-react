import { useStatusClass, useStatusColor } from 'features/common/Hooks';
import { KPI } from 'model/DashboardModel';
import React, { useState } from 'react';
import styles from './Kpi.module.scss';
import { useDispatch } from "react-redux";
import { dashboardActions } from "../../dashboardSlice";

type KpiProps = {
  kpi: KPI;
  kpiIndex: number;
  ciId: string;
};

const Kpi: React.FC<KpiProps> = (props: any) => {

  const { kpiIndex, ciId } = props;
  const dispatch = useDispatch();

  const selectKpi = (e: React.MouseEvent) => {
    dispatch(dashboardActions.selectKpi({ cardId: ciId, kpiIndex }));
  };

  const isKpiSelected = () => {
    return props.kpi.isActive;
  };

  const statusClass = useStatusClass(props.kpi.status);
  const statusColor = useStatusColor(props.kpi.status);

  const iconColor = isKpiSelected() ? '#242C2E' : statusColor;

  return (
    <div non-draggable="true" className={`${styles.kpi} ${statusClass} ${isKpiSelected() ? styles.selected : ''}`}
         onClick={selectKpi} >
      {props.children()(iconColor)}
    </div>
  );
};

export default Kpi;
