import { CardHolder } from "features/dashboard/сomponents/card/CardHolder";
import React, { CSSProperties, useRef } from "react";
import useDimensions from "./DimensionsHook";
import useDragAndDrop from "./DragAndDropHook";
import { GridItem } from "./ToolbarGrid";
import { CardDataLoader } from "../dashboard/сomponents/card/context/CardContext";

type GridProps = {
  columnGap?: number;
  id?: string;
  itemIds: string[];
  viewId: string;
};

export const DragAndDropGrid: React.FC<GridProps> = (props) => {
  const gridStyle: CSSProperties = {
    display: "grid",
    columnGap: 20,
    rowGap: 20,
    marginLeft: "150px",
    marginRight: "150px",
    gridTemplateRows: "minmax(94px, 1fr)",
    gridTemplateColumns: "repeat(12,minmax(10px,1fr))",
    alignItems: "center",
  };

  const gridRef = useRef<HTMLDivElement>(null);
  const dimensionsState = useDimensions(gridRef, props.itemIds);
  const {
    handleMouseDown,
    handleMouseMove,
    handleMouseUp,
  } = useDragAndDrop(dimensionsState, props.viewId);

  return (
    <div
      style={gridStyle}
      id={props.id}
      ref={gridRef}
      onMouseDown={handleMouseDown}
      onMouseMove={handleMouseMove}
      onMouseUp={handleMouseUp}
    >
      {props.itemIds.map((id: string, index: number) => {
        return (
          <GridItem columnSize={2} key={id}>
            <CardDataLoader cardId={id}
                            cardIndex={index}>
              <CardHolder/>
            </CardDataLoader>
          </GridItem>
        );
      })}
    </div>
  );
};
