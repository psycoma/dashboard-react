import { DimensionsState, ItemDimension } from "model/DashboardModel";
import { RefObject, useEffect, useState } from "react";
import useResizeObserver from "use-resize-observer";

export const calculateDimensionsState = (
  ids: Array<string>,
  gridCoordinates: { height: number; width: number },
  cardCoordinates: DOMRect
): DimensionsState => {
  const gutter = 20;

  const itemsPerRow = Math.floor(gridCoordinates.width / cardCoordinates.width);

  const dimensions = Array<ItemDimension>();

  for (let i = 0; i < ids.length; i++) {
    dimensions.push({
      id: ids[i],
      x: i * cardCoordinates.width + cardCoordinates.x + gutter,
      y: cardCoordinates.y,
      height: cardCoordinates.height,
      width: cardCoordinates.width,
    });
  }

  return dimensions;
};

const useDimensions = (
  gridRef: RefObject<HTMLDivElement>,
  cardsIds: string[]
) => {
  const { width = 1, height = 1 } = useResizeObserver<HTMLDivElement>({
    ref: gridRef,
  });

  const [dimensionsState, setDimensionsState] = useState<DimensionsState>([]);

  useEffect(() => {
    if (!gridRef.current?.firstChild) {
      return;
    }

    const firstCardCoordinates = (gridRef.current
      .firstChild as Element).getBoundingClientRect();

    setDimensionsState(
      calculateDimensionsState(
        cardsIds,
        { width, height },
        firstCardCoordinates
      )
    );
  }, [width, height, gridRef, cardsIds]);

  return dimensionsState;
  
};

export default useDimensions;
