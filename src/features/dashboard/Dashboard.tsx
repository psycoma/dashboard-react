import { DragAndDropGrid } from "features/grid/DragAndDropGrid";
import { DashboardGrid } from "features/grid/ToolbarGrid";
import React from "react";
import { useSelector } from "react-redux";
import styles from "./Dashboard.module.scss";
import DashboardMenu from "features/menu/DashboardMenu";
import { currentViewContentSelector } from "./dashboardSliceSelectors";

const Dashboard = () => {
  const dashboardState = useSelector(currentViewContentSelector);

  if (!dashboardState) {
    return null;
  }
  //todo refactor selector
  const { cards, view: { id: viewId } } = dashboardState;
  const itemIds = cards.map(card => card.id);

  return (
    <div className={styles.dashboard}>
      <DashboardGrid>
        <DashboardMenu/>
      </DashboardGrid>
      <DragAndDropGrid id="#dashboardGrid" itemIds={itemIds} viewId={viewId}/>
    </div>
  );
};

export default Dashboard;
